#!/bin/bash

sudo apt-get update

# 1) basic packages
sudo apt-get install ssh vim git tree realpath
git config --global core.editor "vim"

# 2) Install dpkg-dev, g++, gcc, libc6-dev, make by install build-essential (see ref: http://packages.ubuntu.com/precise/build-essential)
sudo apt-get install build-essential 

# 3) install ROS (ref: http://wiki.ros.org/indigo/Installation/Ubuntu)
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://pool.sks-keyservers.net --recv-key 0xB01FA116
sudo apt-get update
sudo apt-get install ros-indigo-desktop-full

# 3-a) Initialize rosdep
sudo rosdep init
rosdep update

# 3-b) Environment setup
echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
source ~/.bashrc

# 3-c) Getting rosinstall
sudo apt-get install python-rosinstall

# 4) Install catkin_tools (see ref: https://catkin-tools.readthedocs.org/en/latest/installing.html)
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list'
wget http://packages.ros.org/ros.key -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get install python-catkin-tools

# 5) Initializing a new catkin workspace for yamaha-classify (see ref: https://catkin-tools.readthedocs.org/en/latest/quick_start.html#initializing-a-new-workspace)
ws=~/catkin_workspace
mkdir -p $ws/src
cd $ws
catkin init

# 6) Install CUDA
mkdir -p ~/Local
cd ~/Local
wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1404/x86_64/cuda-repo-ubuntu1404_7.5-18_amd64.deb
sudo dpkg -i cuda-repo-ubuntu1404_7.5-18_amd64.deb
sudo apt-get update
sudo apt-get install cuda

# 6-a) Install cuDNN
cd ~/Local
wget http://frc.ri.cmu.edu/~poweic/cudnn-7.0-linux-x64-v4.0-rc.tgz
tar xvf cudnn-7.0-linux-x64-v4.0-rc.tgz
sudo cp cuda/lib64/* /usr/local/cuda/lib64/
sudo cp cuda/include/* /usr/local/cuda/include/
echo "export PATH=/usr/local/cuda/bin:$PATH" >> ~/.bashrc
echo "export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH" >> ~/.bashrc
cd ~/
wget http://frc.ri.cmu.edu/~poweic/.theanorc
source ~/.bashrc

# 7) Install Theano (see ref: http://deeplearning.net/software/theano/install_ubuntu.html#install-ubuntu)
sudo apt-get install python-scipy python-dev python-pip python-nose libopenblas-dev
sudo pip install numpy
sudo pip install Theano
sudo apt-get install ipython

# 8) Install Lasagne (see ref: http://lasagne.readthedocs.org/en/latest/user/installation.html)
git clone https://github.com/Lasagne/Lasagne.git
cd Lasagne
sudo pip install -r requirements.txt
sudo python setup.py install

# 9) Install all dependencies for mavs_dnn (or classification)
# Cache git password so that when cloning repositories from bitbucket, you won't be asked for password over and over again.
cd $ws/src
git config --global credential.helper cache
git config --global credential.helper 'cache --timeout=3600' # 1 hour = 3600 seconds

git clone https://github.com/catkin/catkin_simple.git
for repo_name in cython_module depth_to_cloud geom_cast geom_cast_extra numpy_eigen pcl_util pydnn pygeom pyhtml pyimg pyml_util pymsg pypc python_module tictoc_profiler pyprof; do
  git clone https://poweic@bitbucket.org/castacks/${repo_name}.git
done

git clone https://poweic@bitbucket.org/cmu_yamaha/classification.git
mv classification mavs_dnn

sudo apt-get install libhdf5-dev
sudo pip install cython h5py path.py arrow msgpack-python msgpack_numpy scikit-image scikit-learn seaborn treedict pandas

# install pylearn2
cd ~/Local
git clone git://github.com/lisa-lab/pylearn2.git
cd pylearn2
python setup.py develop

# install colorcorrect.algorithm
cd ~/Local
wget https://pypi.python.org/packages/source/c/colorcorrect/colorcorrect-0.05.tar.gz
tar zxvf colorcorrect-0.05.tar.gz
cd colorcorrect-0.05/
sudo python setup.py build
sudo python setup.py install

cd $ws
catkin build
echo "source $ws/devel/setup.bash" >> ~/.bashrc
source ~/.bashrc
